/**
 * @type {import('gatsby').GatsbyConfig}
 */
module.exports = {
  siteMetadata: {
    title: `my-static-website`,
    siteUrl: `https://www.yourdomain.tld`,
  },
  plugins: [],
}
